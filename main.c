#include <stdio.h>
#include <pthread.h>
#include <lcm/lcm.h>

#define EXTERN
#include "bbb.h"
#include "camera.h"
#include "servo.h"
#include "guidance.h"
#include "ultrasonic.h"
#include "imu.h"
#include "comm.h"


extern servo_commands_t cmds;
extern unsigned char CMD;

extern double gyro[3];
extern double accel[3];
extern unsigned char grabbed;

extern GRAB_T grabSM; //Grab state machine
extern GRAB_T dropSM; //Drop state machine

pthread_mutex_t ballpos_mutex;
pthread_mutex_t go_target_mutex;
pthread_mutex_t imu_data_mutex;
pthread_mutex_t gs_command;
pthread_mutex_t stable_mutex;
pthread_mutex_t servo_mutex;


int main(int argc,char **argv){

    imu_hd_t imu_data;

    pthread_t pixy_thread;
    pthread_t imu_thread;
    pthread_t log_thread;
    pthread_t comm_thread;

    unsigned char state;

    
    //Initialize BBB
    if (bbb_init()) {
	printf("Error initializing BBB.\n");
	return -1;
    }

    if (bbb_initADC()) {
	printf("Error initializing BBB ADC.\n");
	return -1;
    }

    imu_data = IMU_init();
    

    pthread_create (&pixy_thread, NULL, pixy_daq, NULL);
    pthread_create (&imu_thread, NULL, imu_daq, &imu_data);
    pthread_create (&log_thread, NULL, logdata, NULL);
    pthread_create (&comm_thread, NULL, comm, NULL);
        
    pthread_mutex_init(&ballpos_mutex, NULL);
    pthread_mutex_init(&go_target_mutex, NULL);
    pthread_mutex_init(&imu_data_mutex, NULL);
    pthread_mutex_init(&stable_mutex, NULL);
    pthread_mutex_init(&gs_command, NULL);
    pthread_mutex_init(&servo_mutex, NULL);

    cmds.s1 = atof(argv[1]);
    cmds.s2 = atof(argv[2]);
    cmds.s3 = atof(argv[3]);
    cmds.s4 = atof(argv[4]);

    ServoInit();
    
    SetServoAngle();

    usleep(1000);

    while(1){

	pthread_mutex_lock(&gs_command);
	state = CMD;
	pthread_mutex_unlock(&gs_command);

       
	if(state == 0){	    
	    Stow();
	    grabSM.state = 0;
	    dropSM.state = 0;
	}
	else if(state == 1){
	    Grab();
	    dropSM.state = 0;
	}
	else if(state == 2){
	    Drop();
	    grabSM.state = 0;
	}
	else if(state == 9){
	    StopPWM();
	    break;
	}

	SetServoAngle();
	
    }


    pthread_join (pixy_thread, NULL);
    
    return 0;
}
