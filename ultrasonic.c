#define EXTERN extern
#include "ultrasonic.h"

#define MY_PORT "/dev/ttyO1"  // BB-UART1
#define MY_BAUD 9600
#define MY_BLOCKING  0   // Set to non-blocking (0)

int ConfigUltraSonic(){

    int serfd;

    serfd = serial_open(MY_PORT, MY_BAUD, MY_BLOCKING); 
    
    if (serfd<0) {
    printf("Error opening serial port.\n");
    return 0;
    }
    
    // Change to have odd parity (1); even = 2, none = 0
    serial_set_mode(serfd, 8, 0, 1);
    
    return serfd;
}

double ReadUltraSonic_Serial(int fd){

    
    return 0;

}

double ReadUltraSonic_AN(){

    double height;

    int adc_value = bbb_readADC(AIN0);
    
    height = adc_value/9.8;

    printf("height = %f\n",height);

    return height;

}




