#ifndef GUIDANCE_H
#define GUIDANCE_H

#include <stdio.h>
#include <math.h>
#include <pthread.h>

typedef struct _GRAB_DFSA_ GRAB_T;
struct _GRAB_DFSA_{

    unsigned char state;
    int n[6];
    
};

// Delat arm function declarations
int delta_calcForward(double theta1, double theta2, double theta3, double *x0, double *y0, double *z0);
int delta_calcAngleYZ(double x0, double y0, double z0, double *theta);
int delta_calcInverse(double x0, double y0, double z0, double *theta1, double *theta2, double *theta3);

int GetIKsolution(double *target);

void Grab();

void Stow();

void Drop();

#endif
