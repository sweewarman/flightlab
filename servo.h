#ifndef SERVO_H
#define SERVO_H

#include "bbb.h"

// Struct for commanded servo angles
typedef struct servo_commands servo_commands_t;
struct servo_commands{
    double s1; 
    double s2;
    double s3;
    double s4;
};

/**
 *@brief function to initialize 4 servos
 */

int ServoInit();

/**
 *@brief function set servo angles. 
 * Servo angles are set in a global servo_commands_t struct
 */

unsigned char SetServoAngle();

void ServoClamp(double *s1,double *s2,double *s3,double *s4);

void StopPWM();

#endif
