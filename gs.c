#include <stdio.h>
#include <lcm/lcm.h>
#include "lcmtypes/com_t.h"
#include <inttypes.h>

int main()
{
    lcm_t *lcm = lcm_create(NULL);

    com_t msg = {};

    const char *channel = "COMM";

    int8_t command;

    unsigned char x;

    // Enter read loop
    while (1) {
        
	printf("Enter command\n");
	scanf("%hhu",&x);

	msg.command = (int8_t)x;

	com_t_publish(lcm, channel, &msg);
    }
    
    lcm_destroy(lcm);
}


