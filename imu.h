#ifndef IMU_H
#define IMU_H

#include <unistd.h>
#include <pthread.h>
#include "bbb.h" 


typedef struct imu_hd imu_hd_t;
struct imu_hd{
    struct I2C_data i2cd_gyro;
    struct I2C_data i2cd_accelmag;
};

// Local functions
void read_gyro(struct I2C_data *i2cdata, double g[]);
void read_accel(struct I2C_data *i2cdata, double a[]);
void read_mag(struct I2C_data *i2cdata, double m[]);
void read_temp(struct I2C_data *i2cdata, double *temp);

imu_hd_t IMU_init();

void* imu_daq(void * data);

// Define gyro bias data
// ADJUST THESE FOR YOUR IMU (see read_gyro() code below)
#define GYROX_BIAS  130
#define GYROY_BIAS  -35
#define GYROZ_BIAS  750

#endif
