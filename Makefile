# Add your targets (i.e. the name of your output program) here
BINARIES = pixy_driver pixy_example
BBBLIB_OBJ  = bbb_adc.o bbb_gpio.o bbb_i2c.o bbb_init.o bbb_pwm.o 

CC = gcc
CFLAGS = -g -Wall -std=gnu99 -lgsl -lgslcblas `pkg-config --cflags lcm`
LDFLAGS = `pkg-config --libs lcm`
BINARIES := $(addprefix bin/,$(BINARIES))

BBBLIB_OBJ := $(addprefix bbblib/,$(BBBLIB_OBJ))

FLIGHT_SRC = main.o servo.o guidance.o camera.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o \
	serial.o ultrasonic.o filter.o imu.o lcmtypes/daq_t.o util.o comm.o lcmtypes/com_t.o

.PHONY: all clean lcmtypes bbblib

all: lcmtypes bbblib $(BINARIES)

lcmtypes:
	@echo "\n"
	@$(MAKE) -C lcmtypes

bbblib:
	@echo "\n"
	@$(MAKE) -C bbblib

clean:
	@$(MAKE) -C lcmtypes clean
	@$(MAKE) -C bbblib clean
	rm -f *~ *.o bin/* 

cleanf:
	rm -f $(FLIGHT_SRC) flight


%.o: %.c
	@echo "\n"
	$(CC) $(CFLAGS) -c $^ -o $@ -I./bbblib

lcmtypes/%.o: lcmtypes/%.c
	@echo "\n"
	$(CC) $(CFLAGS) -c $^ -o $@


bin/pixy_driver: pixy_driver.o util.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	@echo "\n"
	$(CC) -o $@ $^ $(LDFLAGS)

bin/pixy_example: pixy_example.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	@echo "\n"
	$(CC) -o $@ $^ $(LDFLAGS)

# Add build commands for your targets here
flight: $(FLIGHT_SRC) $(BBBLIB_OBJ) 
	@echo "\n"
	$(CC) -o $@ $^ $(LDFLAGS) -I./bbblib -lm -lgsl -lgslcblas
	@echo "\n"
