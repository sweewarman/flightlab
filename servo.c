
#define EXTERN extern
#include "servo.h"

#define DCYCLE_MIN 3.0        //Minimum duty cycle percentage for servo (0 degrees)
#define DCYCLE_MAX 16.07      //Maximum duty cycle percentage
#define PERIOD     16000000   //PWM period in nano seconds

//Servo bias angles for zero correction
#define SV1_BIAS 5
#define SV2_BIAS -5
#define SV3_BIAS 0
#define SV4_BIAS 0

//Max and Min servo angles for clamp function
#define MAX_ARM_SERVOS  95
#define MIN_ARM_SERVOS -10
#define MAX_EE_SERVOS 50
#define MIN_EE_SERVOS 0

#define ZERO_POS 40

// Convert pwm to angles
#define ANGLE_2_PWM(ANGLE) DCYCLE_MIN + (DCYCLE_MAX - DCYCLE_MIN)/180*ANGLE

extern pthread_mutex_t servo_mutex;

servo_commands_t cmds;

int ServoInit(){

    //Initialize PWM ports on BBB
    if (bbb_initPWM(0)) {
	printf("Error initializing BBB PWM pin %d.\n", PWM_SV1);
	return -1;
    }

    if (bbb_initPWM(1)) {
	printf("Error initializing BBB PWM pin %d.\n", PWM_SV2);
	return -1;
    }

    if (bbb_initPWM(2)) {
	printf("Error initializing BBB PWM pin %d.\n", PWM_SV3);
	return -1;
    }

    if (bbb_initPWM(3)) {
	printf("Error initializing BBB PWM pin %d.\n", PWM_SV4);
	return -1;
    }


    //Setup the Period, Duty cycle
    bbb_setPeriodPWM(0, PERIOD);  // Period is in nanoseconds
    bbb_setPeriodPWM(1, PERIOD);  // Period is in nanoseconds
    bbb_setPeriodPWM(2, PERIOD);
    bbb_setPeriodPWM(3, PERIOD);

    bbb_setDutyPWM(0, DCYCLE_MIN);
    bbb_setDutyPWM(1, DCYCLE_MIN);
    bbb_setDutyPWM(2, DCYCLE_MIN);
    bbb_setDutyPWM(3, DCYCLE_MIN);

    bbb_setRunStatePWM(0, pwm_run);
    bbb_setRunStatePWM(1, pwm_run);
    bbb_setRunStatePWM(2, pwm_run);
    bbb_setRunStatePWM(3, pwm_run);
    
    return 0;

}


unsigned char SetServoAngle(){

    //Convert angles to PWM values
    pthread_mutex_lock(&servo_mutex);
    double angle1 = ZERO_POS + cmds.s1 + SV1_BIAS;
    double angle2 = ZERO_POS + cmds.s2 + SV2_BIAS;
    double angle3 = ZERO_POS + cmds.s3 + SV3_BIAS; 
    double angle4 = ZERO_POS + cmds.s4 + SV4_BIAS;
    pthread_mutex_unlock(&servo_mutex);

    //ServoClamp(&angle1,&angle2,&angle3,&angle4);

    // Calculate Duty cycle from angles
    double pwm1   = ANGLE_2_PWM(angle1);
    double pwm2   = ANGLE_2_PWM(angle2);
    double pwm3   = ANGLE_2_PWM(angle3);
    double pwm4   = ANGLE_2_PWM(angle4);
  
    
    bbb_setDutyPWM(0, pwm1);
    bbb_setDutyPWM(1, pwm2);
    bbb_setDutyPWM(2, pwm3);
    bbb_setDutyPWM(3, pwm4);

    return 0;

}

void ServoClamp(double *s1,double *s2,double *s3,double *s4){

    if(*s1 > MAX_ARM_SERVOS){
	*s1 = MAX_ARM_SERVOS;
    }
    
    if(*s1 < MIN_ARM_SERVOS){
	*s1 = MIN_ARM_SERVOS;
    }
    
    if(*s2 > MAX_ARM_SERVOS){
	*s2 = MAX_ARM_SERVOS;
    }

    if(*s2 < MIN_ARM_SERVOS){
	*s2 = MIN_ARM_SERVOS;
    }

    if(*s3 > MAX_ARM_SERVOS){
	*s3 = MAX_ARM_SERVOS;
    }

    if(*s3 < MIN_ARM_SERVOS){
	*s3 = MIN_ARM_SERVOS;
    }

    if(*s4 > MAX_EE_SERVOS){
	*s4 = MAX_EE_SERVOS;
    }

    if(*s4 < MIN_EE_SERVOS){
	*s4 = MIN_EE_SERVOS;
    }
}


void StopPWM(){

    bbb_setRunStatePWM(0, pwm_stop);
    bbb_setRunStatePWM(1, pwm_stop);
    bbb_setRunStatePWM(2, pwm_stop);
    bbb_setRunStatePWM(3, pwm_stop);
    

}
