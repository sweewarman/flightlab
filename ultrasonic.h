#ifndef ULTRA_SONIC_H
#define ULTRA_SONIC_H

#include <stdio.h>
#include <string.h>
#include "serial.h"
#include "bbb.h"

int ConfigUltraSonic();

double ReadUltraSonic_Serial(int fd);

double ReadUltraSonic_AN();

#endif
