#include "camera.h"

//const double cc1w[2] = {-10,5};
//const double cc2w[2] = {0,10};
//const double cc3w[2] = {10,5};


const double cc1w[2] = {-11.5,17.5};
const double cc2w[2] = {0,14.0};
const double cc3w[2] = {11.5,17.5};


const double ccw_angle[3] = {0,0,0};
double cc_p1[2];
double cc_p2[2];
double cc_p3[2];

double cc_angles[3];

unsigned char TARGETS_SEEN = 0;

#define CAM_OFF_Y 10
#define CAM_OFF_X 0
#define BALL_HEIGHT 27.0

pp_ball_t ball_pos;

extern pthread_mutex_t ballpos_mutex;
extern pthread_mutex_t go_target_mutex;

double scale_cos = 0;


void* pixy_daq(void *data){

    lcm_t *lcm = lcm_create(NULL);

    pixy_frame_t_subscribe(lcm, "PIXY", pixy_handler, NULL);

    printf("Starting PIXY data acquisition\n");

    // Enter read loop
    while (1) {
        lcm_handle(lcm);
    }

    lcm_destroy(lcm);

}


void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata)
{
    //printf("Received message on channel %s, timestamp %" PRId64 "\n",
    //channel, msg->utime);
    
    double xg,yg;
    
    unsigned char targets[3]={0,0,0};
    
    double rot_angle = 0;

    //printf("Number of objects: %d\n", msg->nobjects);
    for (int i = 0; i < msg->nobjects; i += 1) {
        pixy_t *obj = &msg->objects[i];

	//Converting to ground co-ordinates
	xg = (double)(obj->x);
        yg = (double)(obj->y);

        xg = (xg-160)/160;
	yg = -(yg-100)/100;

        //printf("%d: ", i);
        
	if (obj->type == PIXY_T_TYPE_COLOR_CODE) {

            //printf("Color code %d (octal %o) at (%f, %f) size (%d, %d)"
            //       " angle %d\n", obj->signature, obj->signature,
            //       xg, yg, obj->width, obj->height, obj->angle);
	    

	    if( obj->signature == 013 ){

		//printf("storing color code 13\n");
		cc_p1[0] = xg;
		cc_p1[1] = yg;

		targets[0]    = 1;
		cc_angles[0] = obj->angle; 
	    }

	    if( obj->signature == 014 ){

		//printf("storing color code 14\n");
		cc_p2[0] = xg;
		cc_p2[1] = yg;

		rot_angle = obj->angle;

		targets[1] = 1;
		cc_angles[1] = obj->angle; 
	       
	    }

	    if( obj->signature == 034 ){

		//printf("storing color code 34\n");
		cc_p3[0] = xg;
		cc_p3[1] = yg;

		targets[2] = 1;
		cc_angles[2] = obj->angle; 
	    }


	} else {
            //printf("Signature %d at (%d, %d) size (%d, %d)\n",
	    // obj->signature, obj->x, obj->y, obj->width, obj->height);
        }


	

    }

    double X_target;
    double Y_target;

    double Xq_target = 0;
    double Yq_target = 0;
    double height    = 0;

    //printf("number of color codes = %d\n",n_cc);

    unsigned char sum = targets[0] + targets[1] + targets[2];


    

    if(sum == 3){

	//double dist = distance(cc_p1,cc_p2);

	//printf("pixel distance = %f\n",dist);

	affine_calibration3(cc_p1,cc_p2,cc_p3,&X_target,&Y_target);
       	
	Xq_target =  -(cos(rot_angle*3.142/180)*X_target -sin(rot_angle*3.142/180)*Y_target);

	Yq_target =  -(sin(rot_angle*3.142/180)*X_target +cos(rot_angle*3.142/180)*Y_target);

	//printf("target position = %f,%f\n",Xq_target,Yq_target+CAM_OFF_Y);

	fflush(stdout);

	height = scale_cos/(cos(rot_angle*3.142/180));

	//height = 38.5/24.675508 * height;

	height = 38.5/25.91 * height;

	//height = 29.5*0.628013/11.18 * distance(cc1w,cc2w)/distance(cc_p1,cc_p2);

	//printf("height = %f\n",height);

	pthread_mutex_lock(&ballpos_mutex);
	ball_pos.x  = Xq_target;
	ball_pos.y  = Yq_target + CAM_OFF_Y;
	ball_pos.z  = -(height - BALL_HEIGHT);
	ball_pos.H  = height;
	pthread_mutex_unlock(&ballpos_mutex);

	pthread_mutex_lock(&go_target_mutex);

	if(height < 56){
	    TARGETS_SEEN = 3;
	}
	else{
	    TARGETS_SEEN = 0;
	}
	pthread_mutex_unlock(&go_target_mutex);

    }
    else if(sum==2){
	
	/*
	double target1[2],target2[2];

	double rot_angle1,rot_angle2;

	if(targets[0] && targets[1]){
	    target1[0] = cc_p1[0];
	    target1[1] = cc_p1[1];
	    rot_angle1 = cc_angles[0];

   
	    target2[0] = cc_p2[0];
	    target2[1] = cc_p2[1];
	    rot_angle2 = cc_angles[1];

	    rot_angle  = - (rot_angle1+rot_angle2)/2;
	  
	    printf("target 12\n");
  
	}
	else if(targets[0] && targets[2]){
	    target1[0] = cc_p1[0];
	    target1[1] = cc_p1[1];
	    rot_angle1 = cc_angles[0];

	    target2[0] = cc_p3[0];
	    target2[1] = cc_p3[1];
	    rot_angle2 = cc_angles[2];

	    printf("angle target = %f, real angle %f\n",cc_angles[0],ccw_angle[0]);

	    rot_angle  =  - (rot_angle1+rot_angle2)/2;

	    printf("target 13\n");

	}
	else if(targets[1] && targets[2]){

	    target1[0] = cc_p2[0];
	    target1[1] = cc_p2[1];
	    rot_angle1 = cc_angles[1];

	    target2[0] = cc_p3[0];
	    target2[1] = cc_p3[1];
	    rot_angle2 = cc_angles[2];

	    rot_angle  =  - (rot_angle1+rot_angle2)/2;

	    printf("target 23\n");
	
	}

	affine_calibration2(target1,target2,rot_angle1,rot_angle2,targets,&X_target,&Y_target);
       	
	Xq_target =  -(cos(rot_angle*3.142/180)*X_target + sin(rot_angle*3.142/180)*Y_target);

	Yq_target =  -(-sin(rot_angle*3.142/180)*X_target + cos(rot_angle*3.142/180)*Y_target);

	printf("target position = %f,%f\n",Xq_target,Yq_target+CAM_OFF_Y);


	fflush(stdout);

	height = scale_cos;

	height = 38.5/24.675508 * height;

	printf("rot_angle = %f,height = %f\n",rot_angle,height);

	pthread_mutex_lock(&ballpos_mutex);
	ball_pos.x  = Xq_target;
	ball_pos.y  = Yq_target + CAM_OFF_Y;
	ball_pos.z  = -(height - BALL_HEIGHT-5);
	ball_pos.H  = height;
	pthread_mutex_unlock(&ballpos_mutex);

	pthread_mutex_lock(&go_target_mutex);
	GO_FOR_TARGET = 2;
	pthread_mutex_unlock(&go_target_mutex);
	*/

	pthread_mutex_lock(&go_target_mutex);
	TARGETS_SEEN = 0;
	pthread_mutex_unlock(&go_target_mutex);
	
    }
    else if(sum==1){
	pthread_mutex_lock(&go_target_mutex);
	TARGETS_SEEN = 0;
	pthread_mutex_unlock(&go_target_mutex);

    }
    else{
	pthread_mutex_lock(&go_target_mutex);
	TARGETS_SEEN = 0;
	pthread_mutex_unlock(&go_target_mutex);
    }

}


double Interpolation(double x1,double y1,double x2,double y2,double x){

    double y;

    y = y1 + (y2-y1)/(x2-x1) * (x - x1);
    
    return y;

}

void affine_calibration3(double *cc1p,double *cc2p,double *cc3p,double *X_target,double *Y_target)
{

    const double w_x[3] = {cc1w[0],cc2w[0],cc3w[0]};
    const double w_y[3] = {cc1w[1],cc2w[1],cc3w[1]};
    int i,n=6,m=6; 

    gsl_vector * b = gsl_vector_alloc (n);
    gsl_matrix * A = gsl_matrix_alloc (n,m);

    double pixels[3][2];

    pixels[0][0] = cc1p[0];
    pixels[0][1] = cc1p[1];
    pixels[1][0] = cc2p[0];
    pixels[1][1] = cc2p[1];
    pixels[2][0] = cc3p[0];
    pixels[2][1] = cc3p[1];
    
    
    for (i=0;i<n/2;i++)
    {
	// 2i-th row
	gsl_matrix_set(A,2*i,0,pixels[i][0]);
	gsl_matrix_set(A,2*i,1,pixels[i][1]);
	gsl_matrix_set(A,2*i,2,1);

	gsl_matrix_set(A,2*i,3,0);
	gsl_matrix_set(A,2*i,4,0);
	gsl_matrix_set(A,2*i,5,0);
	// (2i+1)-th row:
	gsl_matrix_set(A,2*i+1,0,0);
	gsl_matrix_set(A,2*i+1,1,0);
	gsl_matrix_set(A,2*i+1,2,0);
	
	gsl_matrix_set(A,2*i+1,3,pixels[i][0]);
	gsl_matrix_set(A,2*i+1,4,pixels[i][1]);
	gsl_matrix_set(A,2*i+1,5,1);

	gsl_vector_set(b,2*i,w_x[i]);
	gsl_vector_set(b,2*i+1,w_y[i]);
    }
    
    gsl_vector * bT = gsl_vector_alloc (m);
    gsl_vector * x  = gsl_vector_alloc (m);
    gsl_matrix * C  = gsl_matrix_alloc (m,m);
    
    gsl_blas_dgemm (CblasTrans, CblasNoTrans,1.0, A, A,0.0, C);
    gsl_blas_dgemv (CblasTrans,1.0, A, b,0.0, bT);  
    gsl_linalg_HH_solve(C,bT,x);
    
    *X_target = gsl_vector_get(x,2);
    *Y_target = gsl_vector_get(x,5);

    scale_cos = gsl_vector_get(x,0);
    
    gsl_matrix_free(C);
    gsl_matrix_free(A);
    gsl_vector_free(b);
    gsl_vector_free(bT);  
    gsl_vector_free(x);
    
}

void affine_calibration2(double *cc1p,double *cc2p,double angle1,double angle2,unsigned char *target,double *X_target,double *Y_target)
{

    double w_x[2];
    double w_y[2];


    int n=4,m=3; 

    gsl_vector * b = gsl_vector_alloc (n);
    gsl_matrix * A = gsl_matrix_alloc (n,m);

    double P1[2],P2[2];

    P1[0] = cc1p[0];
    P1[1] = cc1p[1];
    P2[0] = cc2p[0];
    P2[1] = cc2p[1];
    
   
    if(target[0] == 1 && target[1] == 1){
	w_x[0] = cc1w[0];
	w_x[1] = cc2w[0];
	


	w_y[0] = cc1w[1];
	w_y[1] = cc2w[1];
	
    }
    else if(target[0] == 1 && target[2] == 1){
	w_x[0] = cc1w[0];
	w_x[1] = cc3w[0];
	

	w_y[0] = cc1w[1];
	w_y[1] = cc3w[1];
	
    }
    else if(target[1] == 1 && target[2] == 1){
	w_x[0] = cc2w[0];
	w_x[1] = cc3w[0];
	

	w_y[0] = cc2w[1];
	w_y[1] = cc3w[1];
	
    }

    double a1 = P1[0]*cos(-angle1*3.142/180) - 
	P1[1]*sin(-angle1*3.142/180);

    double a2 = P1[0]*sin(-angle1*3.142/180) + 
	P1[1]*cos(-angle1*3.142/180);

    double a3 = P2[0]*cos(-angle2*3.142/180) - 
	P2[1]*sin(-angle2*3.142/180);

    double a4 = P2[0]*sin(-angle2*3.142/180) + 
	P2[1]*cos(-angle2*3.142/180);

    gsl_matrix_set(A,0,0,a1);
    gsl_matrix_set(A,0,1,1);
    gsl_matrix_set(A,0,2,0);

    gsl_matrix_set(A,1,0,a2);
    gsl_matrix_set(A,1,1,0);
    gsl_matrix_set(A,1,2,1);

    gsl_matrix_set(A,2,0,a3);
    gsl_matrix_set(A,2,1,1);
    gsl_matrix_set(A,2,2,0);

    gsl_matrix_set(A,3,0,a4);
    gsl_matrix_set(A,3,1,0);
    gsl_matrix_set(A,3,2,1);
    
    gsl_vector_set(b,0,w_x[0]);
    gsl_vector_set(b,1,w_y[0]);
    gsl_vector_set(b,2,w_x[1]);
    gsl_vector_set(b,3,w_y[1]);
    
    gsl_vector * bT = gsl_vector_alloc (m);
    gsl_vector * x  = gsl_vector_alloc (m);
    gsl_matrix * C  = gsl_matrix_alloc (m,m);
    
    gsl_blas_dgemm (CblasTrans, CblasNoTrans,1.0, A, A,0.0, C);
    gsl_blas_dgemv (CblasTrans,1.0, A, b,0.0, bT);  
    gsl_linalg_HH_solve(C,bT,x);
    

    scale_cos = gsl_vector_get(x,0);

    *X_target = gsl_vector_get(x,1);
    *Y_target = gsl_vector_get(x,2);

    gsl_matrix_free(C);
    gsl_matrix_free(A);
    gsl_vector_free(b);
    gsl_vector_free(bT);  
    gsl_vector_free(x);
    
}

double distance(double *p1,double *p2){
    
    double y;

    y = sqrt(pow( (p2[0] - p1[0]),2 ) + pow( (p2[1] - p1[1]),2 ));


    return y;


}
