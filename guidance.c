#define EXTERN extern
#include "guidance.h"
#include "servo.h"
#include "filter.h"
#include "camera.h"
#include "util.h"

extern pp_ball_t ball_pos;

extern pthread_mutex_t ballpos_mutex;
extern pthread_mutex_t go_target_mutex;
extern pthread_mutex_t imu_data_mutex;
extern pthread_mutex_t stable_mutex;
extern pthread_mutex_t servo_mutex;

extern unsigned char TARGETS_SEEN;

extern servo_commands_t cmds;

extern unsigned char quad_stable;



med_filter_t target_filt;

GRAB_T grabSM; //Grab state machine
GRAB_T dropSM; //Drop state machine

#define WAIT1 750000
#define WAIT2 360000
#define WAIT3 850000

#define TOTAL_GSM 6

mvag_filter_t cmds_s1;
mvag_filter_t cmds_s2;
mvag_filter_t cmds_s3;

unsigned int n1 = 0;
unsigned int n2 = 0;
unsigned char grabbed = 0;

int64_t time_start1 = 0;
int64_t time_now1   = 0;
int64_t time_diff1  = 0;

int64_t time_start2 = 0;
int64_t time_now2   = 0;
int64_t time_diff2  = 0;

////////////

// Robot geometry (assumes identical links) 
// --> watch these globals - may want to add identifiers (e.g., "delta_f", "delta_e"...)
const double f  = 23.407;     // base reference triangle side length
const double e  = 13.015;     // end effector reference triangle side length
const double rf = 9.5;    // Top link length
const double re = 16.5;    // Bottom link length
 
// trigonometric constants
const double pi     = 3.141592653;    // PI
const double sqrt3  = 1.732051;    // sqrt(3)
const double sin120 = 0.866025;   // sqrt(3)/2
const double cos120 = -0.5;        
const double tan60  = 1.732051;    // sqrt(3)
const double sin30  = 0.5;
const double tan30  = 0.57735;     // 1/sqrt(3)

extern servo_commands_t cmds;

// forward kinematics: (theta1, theta2, theta3) -> (x0, y0, z0)
// returned status: 0=OK, -1=non-existing position
int delta_calcForward(double theta1, double theta2, double theta3, double *x0, double *y0, double *z0) 
{
    double t = (f-e)*tan30/2;
    double dtr = pi/(double)180.0;
 
    theta1 *= dtr;
    theta2 *= dtr;
    theta3 *= dtr;
 
    double y1 = -(t + rf*cos(theta1));
    double z1 = -rf*sin(theta1);
 
    double y2 = (t + rf*cos(theta2))*sin30;
    double x2 = y2*tan60;
    double z2 = -rf*sin(theta2);
 
    double y3 = (t + rf*cos(theta3))*sin30;
    double x3 = -y3*tan60;
    double z3 = -rf*sin(theta3);
 
    double dnm = (y2-y1)*x3-(y3-y1)*x2;
 
    double w1 = y1*y1 + z1*z1;
    double w2 = x2*x2 + y2*y2 + z2*z2;
    double w3 = x3*x3 + y3*y3 + z3*z3;
     
    // x = (a1*z + b1)/dnm
    double a1 = (z2-z1)*(y3-y1)-(z3-z1)*(y2-y1);
    double b1 = -((w2-w1)*(y3-y1)-(w3-w1)*(y2-y1))/2.0;
 
    // y = (a2*z + b2)/dnm;
    double a2 = -(z2-z1)*x3+(z3-z1)*x2;
    double b2 = ((w2-w1)*x3 - (w3-w1)*x2)/2.0;
 
    // a*z^2 + b*z + c = 0
    double a = a1*a1 + a2*a2 + dnm*dnm;
    double b = 2*(a1*b1 + a2*(b2-y1*dnm) - z1*dnm*dnm);
    double c = (b2-y1*dnm)*(b2-y1*dnm) + b1*b1 + dnm*dnm*(z1*z1 - re*re);
  
    // discriminant
    double d = b*b - (double)4.0*a*c;
    if (d < 0) return -1; // non-existing point
 
    *z0 = -(double)0.5*(b+sqrt(d))/a;
    *x0 = (a1*(*z0) + b1)/dnm;
    *y0 = (a2*(*z0) + b2)/dnm;
    return 0;
}
 
// inverse kinematics
// helper functions, calculates angle theta1 (for YZ-plane)
int delta_calcAngleYZ(double x0, double y0, double z0, double *theta) {
    double y1 = -0.5 * 0.57735 * f; // f/2 * tg 30
    y0 -= 0.5 * 0.57735    * e;    // shift center to edge
    // z = a + b*y
    double a = (x0*x0 + y0*y0 + z0*z0 +rf*rf - re*re - y1*y1)/(2*z0);
    double b = (y1-y0)/z0;
    // discriminant
    double d = -(a+b*y1)*(a+b*y1)+rf*(b*b*rf+rf); 
    if (d < 0) return -1; // non-existing point
    double yj = (y1 - a*b - sqrt(d))/(b*b + 1); // choosing outer point
    double zj = a + b*yj;
    *theta = 180.0*atan(-zj/(y1 - yj))/pi + ((yj>y1)?180.0:0.0);
    return 0;
}
 
// inverse kinematics: (x0, y0, z0) -> (theta1, theta2, theta3)
// returned status: 0=OK, -1=non-existing position
int delta_calcInverse(double x0, double y0, double z0, double *theta1, double *theta2, double *theta3) {
    *theta1 = *theta2 = *theta3 = 0;
    int status = delta_calcAngleYZ(x0, y0, z0, theta1);
    if (status == 0) status = delta_calcAngleYZ(x0*cos120 + y0*sin120, y0*cos120-x0*sin120, z0, theta2);  // rotate coords to +120 deg
    if (status == 0) status = delta_calcAngleYZ(x0*cos120 - y0*sin120, y0*cos120+x0*sin120, z0, theta3);  // rotate coords to -120 deg
    return status;
}


int GetIKsolution(double *target){

    double theta1,theta2,theta3;

    int status;

    status = delta_calcInverse(target[0],target[1],target[2],&theta1,&theta2,&theta3);

    if( status || isnan(theta1) || isnan(theta2) || isnan(theta3) ){
	return -1;
    }
    else{
	cmds.s1 = theta1;
	cmds.s2 = theta2;
	cmds.s3 = theta3;
    }

    return 0;
    
}


void Grab(){

    double target[3];
    unsigned char targets_seen = 0;
    unsigned char stable;

    int status;
   
    pthread_mutex_lock(&ballpos_mutex);
    target[0] = ball_pos.x;
    target[1] = ball_pos.y;
    target[2] = ball_pos.z;
    pthread_mutex_unlock(&ballpos_mutex);
    
    pthread_mutex_lock(&go_target_mutex);
    targets_seen = TARGETS_SEEN;
    pthread_mutex_unlock(&go_target_mutex);
	
    pthread_mutex_unlock(&stable_mutex);
    stable = quad_stable;
    pthread_mutex_unlock(&stable_mutex);

    target_filt.window = 5;
    targets_seen  = median_filter(targets_seen,&target_filt);


    if(grabSM.state == 0){

	//Seeing 0 targets
	
	//state entry function
	if(!grabSM.n[0]){
	    memset(grabSM.n,0,sizeof(int)*TOTAL_GSM);
	    grabSM.n[0] = 1;
	}

	pthread_mutex_lock(&servo_mutex);
	cmds.s1 = 0;
	cmds.s2 = 0;
	cmds.s3 = 0;
	cmds.s4 = -20;
	pthread_mutex_unlock(&servo_mutex);
	
	if(targets_seen == 1){
	    grabSM.state = 1;
	}
	else if(targets_seen == 2){
	    grabSM.state = 2;
	}
        else if(targets_seen == 3){
	    grabSM.state = 3;
	}


    }
    else if(grabSM.state  == 1){
	
	//Seeing 1 target


	if(!grabSM.n[1]){
	    memset(grabSM.n,0,sizeof(int)*TOTAL_GSM);
	    grabSM.n[1] = 1;
	}

	pthread_mutex_lock(&servo_mutex);
	cmds.s1 = 0;
	cmds.s2 = 0;
	cmds.s3 = 0;
	cmds.s4 = -20;
	pthread_mutex_unlock(&servo_mutex);

	
	if(targets_seen == 1)
	    grabSM.state = 1;
	else if(targets_seen == 2)
	    grabSM.state = 2;
        else if(targets_seen == 3)
	    grabSM.state = 3;

    }
    else if(grabSM.state == 2){
	
	//Seeing 2 targets

	if(!grabSM.n[2]){
	    memset(grabSM.n,0,sizeof(int)*TOTAL_GSM);
	    grabSM.n[2] = 1;
	}

	
	if(targets_seen == 1)
	    grabSM.state = 1;
	else if(targets_seen == 2)
	    grabSM.state = 2;
        else if(targets_seen == 3)
	    grabSM.state = 3;

    }
    else if(grabSM.state == 3){
	
	//Seeing 3 targets with tracking active

	if(!grabSM.n[3]){
	    memset(grabSM.n,0,sizeof(int)*TOTAL_GSM);
	    grabSM.n[3] = 1;
	    
	    time_start1 = utime_now();
	}

	
			
	status = GetIKsolution(target);

	if(status == 0 && targets_seen == 3){
	    time_now1  = utime_now();
	}
	else{
	    time_now1  = time_start1;
	}

	time_diff1 = time_now1 - time_start1;
	

	
	//printf("status = %d,targets = %d,time diff = %"PRId64"\n",status,targets_seen,time_diff1);
	if(targets_seen == 0)
	    grabSM.state = 2;
	else if(targets_seen == 1)
	    grabSM.state = 1;
	else if(targets_seen == 2)
	    grabSM.state = 2;
        //else if(targets_seen == 3)
	//grabSM.state = 3;
	else if(targets_seen == 3 && time_diff1 > WAIT1){
	    grabSM.state = 4;
	}

    }
    else if(grabSM.state == 4){

	//Retrieve while still tracking 
	
	if(!grabSM.n[4]){
	    memset(grabSM.n,0,sizeof(int)*TOTAL_GSM);
	    grabSM.n[4] = 1;
	    
	    time_start2 = utime_now();
	}

	time_now2 = utime_now();
	time_diff2 = time_now2 - time_start2;	

	GetIKsolution(target);

	pthread_mutex_lock(&servo_mutex);
	cmds.s4 = 50;
	pthread_mutex_unlock(&servo_mutex);

	//printf("closed\n");

	if(time_diff2 > WAIT2){
	    grabSM.state = 5;
	}
	

    }
    else if(grabSM.state == 5){

	//Stow with ball

	if(!grabSM.n[5]){
	    memset(grabSM.n,0,sizeof(int)*TOTAL_GSM);
	    grabSM.n[5] = 1;
	    
	    
	}

	pthread_mutex_lock(&servo_mutex);
	cmds.s1 = -30;
	cmds.s2 = -30;
	cmds.s3 = -30;
	cmds.s4 =  50;
	pthread_mutex_unlock(&servo_mutex);

    }

    
    //cmds.s1 = mv_avg_filter(cmds.s1,&cmds_s1);    
    //cmds.s2 = mv_avg_filter(cmds.s2,&cmds_s2);    
    //cmds.s3 = mv_avg_filter(cmds.s3,&cmds_s3);   

}

void Stow(){
    
    pthread_mutex_lock(&servo_mutex);
    cmds.s1 = -30;
    cmds.s2 = -30;
    cmds.s3 = -30;
    cmds.s4 = -20;
    pthread_mutex_unlock(&servo_mutex);

}

void Drop(){


    double target[3];
    
    unsigned char targets_seen = 0;

    unsigned char stable;
   
    int status;

    pthread_mutex_lock(&ballpos_mutex);
    target[0] = ball_pos.x;
    target[1] = ball_pos.y;
    target[2] = ball_pos.z;
    pthread_mutex_unlock(&ballpos_mutex);
    
    pthread_mutex_lock(&go_target_mutex);
    targets_seen = TARGETS_SEEN;
    pthread_mutex_unlock(&go_target_mutex);
	
    pthread_mutex_unlock(&imu_data_mutex);
    stable = quad_stable;
    pthread_mutex_unlock(&imu_data_mutex);

    target_filt.window = 5;
    targets_seen  = median_filter(targets_seen,&target_filt);

    if(dropSM.state == 0){

	//Seeing 0 targets
	
	//state entry function
	if(!dropSM.n[0]){
	    memset(dropSM.n,0,sizeof(int)*TOTAL_GSM);
	    dropSM.n[0] = 1;
	}

	pthread_mutex_lock(&servo_mutex);
	cmds.s1 = 0;
	cmds.s2 = 0;
	cmds.s3 = 0;
	cmds.s4 = 50;
	pthread_mutex_unlock(&servo_mutex);

	if(targets_seen == 1){
	    dropSM.state = 1;
	}
	else if(targets_seen == 2){
	    dropSM.state = 2;
	}
        else if(targets_seen == 3){
	    dropSM.state = 3;
	}


    }
    else if(dropSM.state  == 1){
	
	//Seeing 1 target


	if(!dropSM.n[1]){
	    memset(dropSM.n,0,sizeof(int)*TOTAL_GSM);
	    dropSM.n[1] = 1;
	}

	pthread_mutex_lock(&servo_mutex);
	cmds.s1 = 0;
	cmds.s2 = 0;
	cmds.s3 = 0;
	cmds.s4 = 50;
	pthread_mutex_unlock(&servo_mutex);

	if(targets_seen == 1)
	    dropSM.state = 1;
	else if(targets_seen == 2)
	    dropSM.state = 2;
        else if(targets_seen == 3)
	    dropSM.state = 3;

    }
    else if(dropSM.state == 2){
	
	//Seeing 2 targets

	if(!dropSM.n[2]){
	    memset(dropSM.n,0,sizeof(int)*TOTAL_GSM);
	    dropSM.n[2] = 1;
	}

	
	if(targets_seen == 1)
	    dropSM.state = 1;
	else if(targets_seen == 2)
	    dropSM.state = 2;
        else if(targets_seen == 3)
	    dropSM.state = 3;

    }
    else if(dropSM.state == 3){
	
	//Seeing 3 targets with tracking active

	if(!dropSM.n[3]){
	    memset(dropSM.n,0,sizeof(int)*TOTAL_GSM);
	    dropSM.n[3] = 1;
	    
	    time_start1 = utime_now();
	}
	
	status = GetIKsolution(target);


	if(status == 0 && targets_seen == 3){
	    time_now1  = utime_now();
	}
	else{
	    time_now1  = time_start1;
	}


	time_diff1 = time_now1 - time_start1;
	//printf("status = %d, targets seen = %d, time now = %"PRId64 "\n",status,targets_seen,time_)

	if(targets_seen == 3 && time_diff1 > WAIT3){
	    dropSM.state = 4;
	}
	else if(targets_seen == 1)
	    dropSM.state = 1;
	else if(targets_seen == 2)
	    dropSM.state = 2;
        else if(targets_seen == 3)
	    dropSM.state = 3;
	

    }
    else if(dropSM.state == 4){

	//drop while still tracking 
	
	if(!dropSM.n[4]){
	    memset(dropSM.n,0,sizeof(int)*TOTAL_GSM);
	    dropSM.n[4] = 1;
	    
	    time_start2 = utime_now();
	}

	time_now2 = utime_now();
	time_diff2 = time_now2 - time_start2;	

	GetIKsolution(target);

	pthread_mutex_lock(&servo_mutex);
	cmds.s4 = -20;
	pthread_mutex_unlock(&servo_mutex);

	if(time_diff2 > WAIT2){
	    dropSM.state = 5;
	}
	

    }
    else if(dropSM.state == 5){

	//Stow without ball

	if(!dropSM.n[5]){
	    memset(dropSM.n,0,sizeof(int)*TOTAL_GSM);
	    dropSM.n[5] = 1;
	    
	    
	}


	pthread_mutex_lock(&servo_mutex);
	cmds.s1 = -30;
	cmds.s2 = -30;
	cmds.s3 = -30;
	cmds.s4 =  -20;
	pthread_mutex_unlock(&servo_mutex);

    }

    
    //cmds.s1 = mv_avg_filter(cmds.s1,&cmds_s1);    
    //cmds.s2 = mv_avg_filter(cmds.s2,&cmds_s2);    
    //cmds.s3 = mv_avg_filter(cmds.s3,&cmds_s3);   


}
