% MATLAB Interface for reading
% LCM log data


%Receive Messages
lc = lcm.lcm.LCM.getSingleton();
aggregator = lcm.lcm.MessageAggregator();

lc.subscribe('PIXY', aggregator);

while true
disp waiting
millis_to_wait = 1000;
msg = aggregator.getNextMessage(millis_to_wait);
if length(msg) > 0
break
end
end

disp(sprintf('channel of received message: %s', char(msg.channel)))
disp(sprintf('raw bytes of received message:'))
disp(sprintf('%d ', msg.data'))

%m = exlcm.pixy_frame_t(msg.data);
%tried many combinations: rob550.pixy_frame_t, pixy_frame_t.msg.data, etc.

disp(sprintf('decoded message:\n'))
%disp([ 'timestamp: ' sprintf('%d ', m.data) ])
disp([ 'timestamp: ' sprintf('%d ', msg.data) ])




%Plot Data

