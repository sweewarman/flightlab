/**
  Serial port handling functions (.h)
  [Prof. Olson / APRIL Lab;
   Adapted for ROB 550, Fall 2014]


  For BeagleBone Black Rev. C:
  1. Open the file /boot/uboot/uEnv.txt.
  2. Add the key "capemgr.enable_partno="
  3. Add ports to enable, comma separated 
     (BB-UART1, BB-UART2, BB-UART4, BB-UART5)
  4. Reboot

  Sample line to add key:
  optargs=capemgr.enable_partno=BB-UART1

  After reboot, you should see device in list:  ls -l /dev/ttyO*
  (for BB-UART1 you should see /dev/ttyO1)
**/

#ifndef _SERIAL_H
#define _SERIAL_H

/** Creates a basic fd:  no flow
    control, no fancy character handling. 
    Configures it for blocking
    reads.  8 data bits, 1 stop bit, no parity.
    Returns the fd, -1 on error
**/
int serial_open(const char *port, int baud, int blocking);

/** Set the baud rate, where the baudrate is just the integer value
    desired.    Returns non-zero on error.
**/
int serial_set_baud(int fd, int baud);

/**
  Sets number of data bits, parity, and stopbits
**/
int serial_set_mode(int fd, int databits, int parity, int stopbits);

/** Set cts/rts flow control.
    Returns non-zero on error.
**/
int serial_set_ctsrts(int fd, int enable);
int serial_set_dtr(int fd, int v);
int serial_set_rts(int fd, int v);

/** Set xon/xoff flow control.
    Returns non-zero on error.
**/
int serial_set_xon(int fd, int enable);

/** Set the port to 8 data bits, 2 stop bits, no parity.
    Returns non-zero on error.
 **/
int serial_set_N82 (int fd);

int serial_close(int fd);

#endif
