#define EXTERN extern
#include "imu.h"
#include "filter.h"

double gyro[3],accel[3],attitude[3];

unsigned char quad_stable = 0;

extern pthread_mutex_t imu_data_mutex;
extern pthread_mutex_t stable_mutex;

#define P_BIAS 1.6
#define Q_BIAS 0.6
#define R_BIAS 0.3

#define P_STABLE 30
#define Q_STABLE 30
#define R_STABLE 100

void* imu_daq(void* data){
    
    double gyro_temp[3],accel_temp[3],attitude_temp[3];
    double gyro_temp2[3];

    imu_hd_t *imu_data = (imu_hd_t*) data;
   
    mvag_filter_t p_filt_mvag;
    mvag_filter_t q_filt_mvag;
    mvag_filter_t r_filt_mvag;
    
    mvag_filter_t ax_filt_mvag;
    mvag_filter_t ay_filt_mvag;
    mvag_filter_t az_filt_mvag;
    
    memset(&p_filt_mvag,0,sizeof(mvag_filter_t));
    memset(&q_filt_mvag,0,sizeof(mvag_filter_t));
    memset(&r_filt_mvag,0,sizeof(mvag_filter_t));

    memset(&ax_filt_mvag,0,sizeof(mvag_filter_t));
    memset(&ay_filt_mvag,0,sizeof(mvag_filter_t));
    memset(&az_filt_mvag,0,sizeof(mvag_filter_t));

    p_filt_mvag.window  = 10;
    q_filt_mvag.window  = 10;
    r_filt_mvag.window  = 10;

    ax_filt_mvag.window = 10;
    ay_filt_mvag.window = 10;
    az_filt_mvag.window = 10;
   
    while(1){
	read_gyro(&imu_data->i2cd_gyro,gyro_temp);
	usleep(100);
	
	read_accel(&imu_data->i2cd_accelmag,accel_temp);
	usleep(100);

	attitude_temp[2] = accel_temp[2];

	gyro_temp2[1]  = -mv_avg_filter(gyro_temp[0],&p_filt_mvag);
	gyro_temp2[0]  = mv_avg_filter(gyro_temp[1],&q_filt_mvag);
	gyro_temp2[2]  = mv_avg_filter(gyro_temp[2],&r_filt_mvag);
	
	accel_temp[0] = mv_avg_filter(accel_temp[0],&ax_filt_mvag);
	accel_temp[1] = mv_avg_filter(accel_temp[1],&ay_filt_mvag);
	accel_temp[2] = mv_avg_filter(accel_temp[2],&az_filt_mvag);

	gyro_temp2[0] = gyro_temp2[0] - P_BIAS;
	gyro_temp2[1] = gyro_temp2[1] - Q_BIAS;
	gyro_temp2[2] = gyro_temp2[2] - R_BIAS;
	
	//attitude_temp[2] = atan(sqrt( pow(accel_temp[0],2) + pow(accel_temp[1],2))/accel_temp[2])  * 180/M_PI;
	attitude_temp[0] = atan(accel_temp[0]/sqrt( pow(accel_temp[1],2) + pow(accel_temp[2],2) )) * 180/M_PI;
	attitude_temp[1] = atan(accel_temp[1]/sqrt( pow(accel_temp[0],2) + pow(accel_temp[2],2) )) * 180/M_PI;

	accel_temp[1] = - accel_temp[1];

	if( fabs(gyro_temp2[0]) <= P_STABLE &&
	    fabs(gyro_temp2[1]) <= Q_STABLE &&
	    fabs(gyro_temp2[2]) <= R_STABLE ){
	    
	    pthread_mutex_lock(&stable_mutex);
	    quad_stable = 1;
	    pthread_mutex_unlock(&stable_mutex);
	}
	else{
	    pthread_mutex_unlock(&stable_mutex);
	    quad_stable = 0;
	    pthread_mutex_unlock(&stable_mutex);
	}

	pthread_mutex_lock(&imu_data_mutex);
	memcpy(gyro,gyro_temp2,sizeof(double)*3);
	memcpy(accel,accel_temp,sizeof(double)*3);
	memcpy(attitude,attitude_temp,sizeof(double)*3);
	pthread_mutex_unlock(&imu_data_mutex);
	
	//printf("imu = %f\n",accel[2]);
    }
}


imu_hd_t IMU_init(){

    struct I2C_data i2cd_gyro, i2cd_accelmag;
   
    byte buf[10];


    imu_hd_t imu_data;

    
    i2cd_gyro.name = I2C_1;  // I2C2 is enumerated as 1 on the BBB unless I2C1 is enabled
    i2cd_gyro.address = 0xD6; // Gyro address (right shifted by 1 to get 7-bit value)
    i2cd_gyro.flags = O_RDWR;  
    
    i2cd_accelmag.name = I2C_1;  // Same I2C port as gyro
    i2cd_accelmag.address = 0x3A; // Accel/Mag address (right shifted by 1 to get 7-bit value)
    i2cd_accelmag.flags = O_RDWR;  
    usleep(1000);
    
    if (bbb_initI2C(&i2cd_gyro)) { // Open I2C fd for gyro
	printf("Error initializing I2C port %d for gyro.\n", (int) (i2cd_gyro.name));
	//return -1;
    }
    usleep(1000);
    
    if (bbb_initI2C(&i2cd_accelmag)) { // Open a second fd (same I2C) for accelmag
	printf("Error initializing I2C port %d for accel.\n", (int) (i2cd_accelmag.name));
	//return -1;
    }
    usleep(1000);
    
    // Activate accelerometer, magnetometer, and gyro
    
    buf[0] = 0x20; buf[1] = 0x0F; 
    bbb_writeI2C(&i2cd_gyro, buf, 2);
    usleep(1000);
    
    // XM control registers: 0x1F - 0x26
    buf[0] = 0x20; buf[1] = 0x67;  // CTRL_REG1_XM:  3=12.5Hz; 7=enable all accel ch.
    bbb_writeI2C(&i2cd_accelmag, buf, 2); 
    usleep(1000);
    buf[0] = 0x24; buf[1] = 0xF0;  // Activate accel, temp through control register 5
    bbb_writeI2C(&i2cd_accelmag, buf, 2); 
    usleep(1000);
    buf[0] = 0x26; buf[1] = 0x00;  // Send 0x00 to control register 7
    bbb_writeI2C(&i2cd_accelmag, buf, 2); 
    usleep(1000);
    
    imu_data.i2cd_gyro     = i2cd_gyro;
    imu_data.i2cd_accelmag = i2cd_accelmag;

    return imu_data;

}


//////////////////////////////////////////
void read_gyro(struct I2C_data *i2cd, double gyro[])
{
  short tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data
  
  buf = 0x28; // X gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &lobyte, 1);
  usleep(1000);
  buf = 0x29; // X gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &hibyte, 1);
  usleep(1000);
  tempint = (((short) hibyte) << 8) | lobyte;
  // GYROX_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your x-gyro
  // printf("gyro x=%hd\n", tempint+GYROX_BIAS); // With your bias this should be near zero
  gyro[0] = 0.00875*(tempint+GYROX_BIAS);  // 110 is the zero bias/offset - please adjust
  
  buf = 0x2A; // Y gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &lobyte, 1);
  usleep(1000);
  buf = 0x2B; // Y gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &hibyte, 1);
  usleep(1000);
  tempint = (((short) hibyte) << 8) | lobyte;
  // GYROY_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your y-gyro
  // printf("gyro y=%hd\n", tempint + GYROY_BIAS); // With your bias this should be near zero
  gyro[1] = 0.00875*(tempint + GYROY_BIAS);  
  
  buf = 0x2C; // Z gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &lobyte, 1);
  usleep(1000);
  buf = 0x2D; // Z gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &hibyte, 1);
  usleep(1000);
  tempint = ((short) hibyte << 8) | lobyte;
  // GYROZ_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your y-gyro
  // printf("gyro z=%hd\n", tempint + GYROZ_BIAS); // With your bias this should be near zero
  gyro[2] = 0.00875*(tempint + GYROZ_BIAS);

  return;
}


void read_accel(struct I2C_data *i2cd, double accel[])
{
  short tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data
  
  buf = 0x28; // X accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x29; // X accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  accel[0] = 0.000061*tempint;    // Not sure about negative readings yet???
  
  buf = 0x2A; // Y accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x2B; // Y accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  accel[1] = 0.000061*tempint;
  
  buf = 0x2C; // Z accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x2D; // Z accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  accel[2] = 0.000061*tempint; 

  return;
}

void read_mag(struct I2C_data *i2cd, double mag[])
{
  short tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data
  
  buf = 0x08; // X mag, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x09; // X mag, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  mag[0] = 0.00008*tempint;
  
  buf = 0x0A; // Y mag, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x0B; // Y mag, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  mag[1] = 0.00008*tempint;
  
  buf = 0x0C; // Z mag, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x0D; // Z mag, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  mag[2] = 0.00008*tempint; 

  return;
}

void read_temp(struct I2C_data *i2cd, double *temp)
{
  short tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data

  buf = 0x05; // Z mag, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x06; // Z mag, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (((short) hibyte) << 8) | lobyte;
  // printf("Temp = %hd\n", tempint);
  *temp = (double) tempint; 

  return;
}
