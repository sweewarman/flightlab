#ifndef CAMERA_H
#define CAMERA_H

#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>
#include <math.h>
#include <pthread.h>

#include "lcmtypes/pixy_t.h"
#include "lcmtypes/pixy_frame_t.h"

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>


typedef struct pp_ball pp_ball_t;
struct pp_ball{

    //Position with respect to center of quadrotr
    double x;   
    double y;
    double z;

    //Height of top plate from color code base;
    double H;
};

void* pixy_daq (void *data);

void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata);

double Interpolation(double x1,double y1,double x2,double y2,double h);


void affine_calibration3(double *cc1p,double *cc2p,double *cc3p,double *X_target,double *Y_target);

void affine_calibration2(double *cc1p,double *cc2p,double angle1,double angle2,unsigned char *targets,double *X_target,double *Y_target);

double distance(double *p1,double *p2);

#endif
